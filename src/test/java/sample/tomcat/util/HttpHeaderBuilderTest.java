package sample.tomcat.util;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class HttpHeaderBuilderTest {
	
	@Test
	public void should_return_httpheaders() {
		HttpHeaders built = new HttpHeaderBuilder().build();
		assertNotNull(built);
		assertTrue(built instanceof HttpHeaders);
	}
	
	@Test
	public void should_return_json_contenttype_httpheader() {
		HttpHeaders built = new HttpHeaderBuilder().setContentType(MediaType.APPLICATION_JSON).build();
		assertTrue(built.getContentType().equals(MediaType.APPLICATION_JSON));
	}
	
}
