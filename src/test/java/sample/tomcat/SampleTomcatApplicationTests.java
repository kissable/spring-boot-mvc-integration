/*
 * Copyright 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sample.tomcat;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import sample.tomcat.service.HelloWorld;
import sample.tomcat.util.HttpHeaderBuilder;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SampleTomcatApplication.class)
@WebIntegrationTest(randomPort = true)
@DirtiesContext
public class SampleTomcatApplicationTests {

	private static final String HTTP_LOCALHOST = "http://localhost:";
	
	@Value("${local.server.port}")
	private int port;

	@Test
	public void testBaseJson() throws Exception {
		final String path = "/json";
		ResponseEntity<HelloWorld> entity = new TestRestTemplate().getForEntity(
				buildUrl(path), HelloWorld.class);
		assertEquals(HttpStatus.OK, entity.getStatusCode());
		assertEquals("Hello World", entity.getBody().getName());
	}

	
	@Test
	public void testBaseXml() throws Exception {
		final String path = "/xml";
		ResponseEntity<HelloWorld> entity = new TestRestTemplate().getForEntity(
				buildUrl(path), HelloWorld.class);
		assertEquals(HttpStatus.OK, entity.getStatusCode());
		assertEquals("Hello World", entity.getBody().getName());
	}
	
	@Test
	public void testJsonName() throws Exception {
		final String name = "test";
		final String path = new StringBuilder().append("/json").append("/name").append("/").append(name).toString();
		ResponseEntity<HelloWorld> entity = new TestRestTemplate().getForEntity(
				buildUrl(path), HelloWorld.class);
		assertEquals(HttpStatus.OK, entity.getStatusCode());
		assertEquals(name, entity.getBody().getName());
	}
	
	@Test
	public void testXmlName() throws Exception {
		final String name = "test";
		final String path = new StringBuilder().append("/xml").append("/name").append("/").append(name).toString();
		ResponseEntity<HelloWorld> entity = new TestRestTemplate().getForEntity(
				buildUrl(path), HelloWorld.class);
		assertEquals(HttpStatus.OK, entity.getStatusCode());
		assertEquals(name, entity.getBody().getName());
	}
	
	@Test
	public void testJsonInput() throws Exception {
		final String path = new StringBuilder().append("/json").append("/input").toString();
		final String responseFix = " response.";
		final String name = "valami";
		
		final HelloWorld hw = new HelloWorld();
		hw.setName(name);
		
		HttpEntity<HelloWorld> request = new HttpEntity<HelloWorld>(hw, 
				new HttpHeaderBuilder().setContentType(MediaType.APPLICATION_JSON).build());	
		
		ResponseEntity<HelloWorld> entity = new TestRestTemplate().postForEntity(buildUrl(path), 
				request
				,  HelloWorld.class);
		assertEquals(HttpStatus.OK, entity.getStatusCode());
		assertEquals(name.concat(responseFix), entity.getBody().getName());
	}
	
	private String buildUrl(final String url) {
		return HTTP_LOCALHOST + this.port + url;
	}

}
