package sample.tomcat.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import sample.tomcat.service.Hello;

public class HelloQueueListener implements MessageListener {

	@Override
	public void onMessage(Message message) {
		if (message instanceof Hello) {
			try {
				System.out.println(message.getStringProperty("description"));
			} catch (final JMSException e) {
				e.printStackTrace();
			}
		}
	}

}
