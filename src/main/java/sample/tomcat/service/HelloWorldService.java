/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sample.tomcat.service;

import javax.jms.Queue;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

@Component("helloWorldService")
public class HelloWorldService {
	
	@Autowired
	@Qualifier("helloInputChannel")
	MessageChannel inputChannel;
	
	@Autowired
	JmsMessageSender jmsMessageSender;
	
	public HelloWorld getHelloMessage() {
		HelloWorld hw = new HelloWorld();
		hw.setName("Hello World");
		hw.setDescription("Hello World desc.");
		return hw;
	}
	
	public void sendHello(final HelloWorld helloWorld) {
		helloWorld.setDescription(" -> send to input channel");
		this.inputChannel.send(new GenericMessage<HelloWorld>(helloWorld));
	}
	
	public HelloWorld processHello(final HelloWorld helloWorld) {
		helloWorld.setDescription(helloWorld.getDescription().concat(" -> process"));
		return helloWorld;
	}
	
	public Hello translate(final HelloWorld helloWorld) {
		final Hello h = new Hello();
		h.setFrom(helloWorld.getName());
		h.setDescription(helloWorld.getDescription().concat(" -> translat"));
		h.setTo("mindegy");
		return h;
	}
	
	public void helloConsumer(final Hello hello) {
		hello.setDescription(hello.getDescription().concat(" -> consume"));
		System.out.println(hello.getFrom() + " - " + hello.getDescription());
		hello.setDescription(hello.getDescription().concat(" -> amq"));
		Queue queue = new ActiveMQQueue("NorbiQueue");
		jmsMessageSender.send(queue, hello.getDescription());
	}
	
}
