package sample.tomcat.service;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

@Service("jmsMessageSender")
public class JmsMessageSender {
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	  public void send(final Destination dest,final String text) {
	    
	    this.jmsTemplate.send(dest,new MessageCreator() {
	      @Override
	      public Message createMessage(Session session) throws JMSException {
	        Message message = session.createTextMessage(text);
	        return message;
	      }
	    });
	  }
	
}
