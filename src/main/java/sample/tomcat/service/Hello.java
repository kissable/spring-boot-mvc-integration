package sample.tomcat.service;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Hello {
	
	private String from;
	
	private String description;
	
	private String to;

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
