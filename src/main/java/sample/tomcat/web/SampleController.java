/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sample.tomcat.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import sample.tomcat.service.HelloWorld;
import sample.tomcat.service.HelloWorldService;

@Controller
public class SampleController {

	@Autowired
	private HelloWorldService helloWorldService;
	
	@RequestMapping(value = "/xml", produces = MediaType.APPLICATION_XML_VALUE)
	@ResponseBody
	public HelloWorld helloWorld() {
		return this.helloWorldService.getHelloMessage();
	}
	
	@RequestMapping(value = "/json", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public HelloWorld helloWorld2() {
		return this.helloWorldService.getHelloMessage();
	}
	
	@RequestMapping(value = "/xml/name/{name}", produces = MediaType.APPLICATION_XML_VALUE ) 
	@ResponseBody   
	public HelloWorld helloWorld3(@PathVariable String name) {
		final HelloWorld hw = new HelloWorld();
		hw.setName(name);
		hw.setDescription(name + " descr.");
		return hw;
	}
	
	@RequestMapping(value = "/json/name/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public HelloWorld helloWorld4(@PathVariable String name) {
		HelloWorld hw = new HelloWorld();
		hw.setName(name);
		hw.setDescription(name + " descr.");
		return hw;
	}
	
	@RequestMapping(value = "/json/input", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody 
	public HelloWorld consumeJson(@RequestBody HelloWorld helloWorld) {
		final HelloWorld hw = new HelloWorld();
		hw.setName(helloWorld.getName() + " response.");
		return hw;
	}
	
	@RequestMapping(value = "/json/amq/in", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public void inputAmq(@RequestBody HelloWorld helloWorld) {
		this.helloWorldService.sendHello(helloWorld);
	}
	
}
