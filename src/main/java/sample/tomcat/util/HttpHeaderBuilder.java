package sample.tomcat.util;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class HttpHeaderBuilder {
	
	private HttpHeaders httpHeaders;

	public HttpHeaderBuilder() {
		if (this.httpHeaders == null) {
			this.httpHeaders = new HttpHeaders();
		}
	}
	
	public HttpHeaders build(){
		return this.httpHeaders;
	}
	
	public HttpHeaderBuilder setContentType(MediaType mediaType) {
		this.httpHeaders.setContentType(mediaType);
		return this;
	}
	
}

