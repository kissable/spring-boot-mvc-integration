package sample.tomcat;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.xml.MarshallingView;

import sample.tomcat.service.HelloWorld;

@Configuration
@EnableAutoConfiguration
public class MVCConfig extends WebMvcConfigurerAdapter {

	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		registry.enableContentNegotiation(xmlViewResolver());
	}
	
	@Bean
	public MarshallingView xmlViewResolver() {
		return new MarshallingView(xmlMarshaller());
	}
	
	@Bean
	public Jaxb2Marshaller xmlMarshaller() {
		Jaxb2Marshaller jm = new Jaxb2Marshaller();
		jm.setClassesToBeBound(HelloWorld.class);
		return jm;
	}
	
}
