# README #

Spring mvc, integration sample project

TODO

* Spring mvc, integration sample project
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* maven package
* java -jar target\gs-spring-boot-0.1.0.jar
* 1. Download and start an ActiveMQ message broker
* 2. login to admin create a queue named NorbiQueue
* 3. start as java program: sample.tomcat.SampleTomcatApplication
* 4. post json {"name":"xyc","description":"wqz"} to http://localhost:8081/json/amq/in
* 5. browse NorbiQueue in ActiveMQ web admin

### spring libs ###

* spring-boot-starter-[web,ws,integraiton,test] 1.2.2.RELEASE

### Who do I talk to? ###

* norbert kiss